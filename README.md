# README #

This is an implementation of category theory in Coq.

### Coq version and Compilation ###

* This development uses features new to Coq8.5
* It has been complied on Debian with Coq 8.5-beta1 and Coq8.5-beta2
* To compile simply type  ``` make ``` -- you will need to have coq_makefile to be on the path